package com.project.controller;

import com.project.model.LeaveCount;
import com.project.model.Person;
import com.project.service.LeaveCountService;
import com.project.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class LeaveCountController {

    @Autowired
    LeaveCountService leaveCountService;

    @Autowired
    PersonService personService;

    @GetMapping("/getAvailableLeavesById")
    public Integer getAvailableLeaves(@RequestParam Integer personId) {
        Person person = personService.getPersonById(personId);
        return person.getLeaveCount().getAvailableLeaves();
    }

    @GetMapping("/getLeaveCountByPersonId")
    public LeaveCount getLeaveCountByPersonId(@RequestParam Integer personId) {
        Person person = personService.getPersonById(personId);
        return person.getLeaveCount();
    }
}
