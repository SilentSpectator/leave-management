package com.project.repository;

import com.project.model.LeaveCount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LeaveCountRepository extends JpaRepository<LeaveCount, Integer> {
}
