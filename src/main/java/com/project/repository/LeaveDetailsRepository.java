package com.project.repository;

import com.project.model.LeaveDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaveDetailsRepository extends JpaRepository<LeaveDetails, Integer> {
}
